﻿#!env python3
import socket, sys, getpass, utils


def build_message(conf):
    boundary = "KkK170891tpbkKk__FV_KKKkkkjjwq"
    msg = []
    msg.append('From: ' + conf.author)
    msg.append('To: ' + ', '.join(conf.receivers))
    msg.append('Subject: ' + conf.subject)
    #msg.append('Date: ' + )
    msg.append('Content-Type:multipart/mixed;boundary="{}"'.format(boundary))
    boundary = '--' + boundary
    msg.append(boundary)
    msg.append('Content-Type:text/plain; charset=utf-8')
    msg.append('')
    msg.append(conf.body)
    attachments = utils.load_attachments(conf.attachments)
    for fname, b64 in attachments:
        msg.append(boundary)
        msg.append('Content-Type:application/octet-stream;name="{}"'.format(fname))
        msg.append('Content-Transfer-Encoding:base64')
        msg.append('Content-Disposition:attachment;filename="{}"'.format(fname))
        msg.append('')
        msg.append(b64.decode())
    msg.append(boundary + '--')
    msg.append('.')
    return '\r\n'.join(msg)



def main(login, password, conf):
    sock = socket.socket()
    sock.connect(conf.addr)
    sock = utils.Sock(sock)
    sock.writeln2('EHLO {}', conf.helo)
    sock.writeln2('STARTTLS')
    sock.starttls()
    sock.writeln2('AUTH LOGIN')
    sock.writeln2(utils.base64encode(login))
    sock.writeln2(utils.base64encode(password))
    sock.writeln2('MAIL FROM:<{}>', conf.author)
    for x in conf.receivers:
        sock.writeln2('RCPT TO:<{}>', x)
    sock.writeln2('DATA')
    message = build_message(conf)
    sock.writeln2(message)
    sock.sock.close()
    
    
if __name__ == '__main__':
    login = input('Login: ')
    passw = getpass.getpass('Password: ')
    conf = utils.load_config('config.txt')
    main(login, passw, conf)