#!env python3
import base64, ssl, getpass, socket


def base64name():
    return 1

def login_password():
    login = input('Login: ')
    passw = getpass.getpass('Password: ')
    return login, passw


def base64encode(s):
    return base64.b64encode(s.encode()).decode()


def load_config(path):
    with open(path, 'r') as f:
        dct = eval(f.read())
        result = lambda: ()
        result.__dict__.update(dct)
        return result


def load_attachments(fnames):
    for fname in fnames:
        with open(fname, 'rb') as file:
            content = file.read()
            yield (fname, base64.b64encode(content))        


class Sock:
    def __init__(self, sock):
        self.sock = sock

    def writeln(self, fmt, *args):
        s = fmt.format(*args) + '\r\n'
        self.sock.send(s.encode())
        return s


    def writeln2(self, fmt, *args, **kwargs):
        s2 = self.writeln(fmt, *args)
        s = s2
        hide_input = kwargs.get('hide_input', False)
        if hide_input:
            s = '*' * len(s) + '\r\n'
        print('>', s, end='')
        p = self.prints(kwargs.get('timeout', 0))
        return (s2, p)
    
    def recvall(self, timeout=0):
        data = b''
        self.sock.settimeout(timeout)
        try:
            while True:
                part = self.sock.recv(4096)
                data += part
        except:
            pass
        self.sock.settimeout(None)
        return data

    def prints(self, timeout=0):
        answ = self.recvall(timeout) \
            if timeout else self.sock.recv(4096)
        to_print = answ.decode('utf8', 'ignore')
        print(to_print, end='')
        return to_print
        
    def starttls(self):
        sec_sock = ssl.wrap_socket(self.sock, ssl_version=ssl.PROTOCOL_SSLv23)
        self.sock = sec_sock
        