#!env python3
import utils, socket, email, mimetypes


def main(login, password, conf):
    sock = socket.socket()
    sock = utils.Sock(sock)
    sock.starttls()
    sock.sock.connect(conf.addr)
    sock.prints()
    sock.writeln2('USER {}', login)
    sock.writeln2('PASS {}', password, hide_input=True)
    _, a = sock.writeln2('LIST', timeout=3)
    a = a.splitlines()
    if not 0 < conf.index < len(a):
        print(':: Required index out of range:', conf.index)
        return
    to_read = int(a[conf.index].split()[1])
    print(':: Message size:', to_read)
    _, msg = sock.writeln2('TOP {} {}', conf.index, conf.top, timeout=3) \
        if conf.top else sock.writeln2('RETR {}', conf.index, timeout=3)
    msg = '\r\n'.join(msg.splitlines()[1:])
    msg = email.message_from_string(msg)
    print(':: From:', msg["From"])
    print(':: To:', msg["To"])
    print(':: Subject:', msg["Subject"])
    if msg.is_multipart():
        for i, part in enumerate(msg.walk()):
            ctype = part.get_content_type()
            ext = mimetypes.guess_extension(ctype)
            if ctype.startswith('multipart/'):
                continue
            bytes = part.get_payload(decode=True)
            if not bytes: 
                continue
            if ctype.startswith('text/'):
                charset = part.get_content_charset() or 'utf8'
                print(':: Body:', bytes.decode(charset, 'ignore'))
            else:
                name = '{}{}'.format(i, ext)
                print(':: Attachment:', name, ctype)
            
                with open(name, 'wb') as f:
                    f.write(part.get_payload(decode=True))
    else:
        charset = msg.get_content_charset() or 'utf8'
        print(':: Body:', msg.get_payload(decode=True).decode(charset, 'ignore'))
    sock.writeln2('QUIT')
    sock.sock.close()
    
    
if __name__ == '__main__':
    login, passw = utils.login_password()
    conf = utils.load_config('pop3_config.txt')
    main(login, passw, conf)