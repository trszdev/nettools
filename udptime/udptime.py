import socket, threading, struct, time

NTP_ORIGIN = ('time.windows.com', 123)
ADDRESS = ('127.0.0.1', 123)
DELTA_SECONDS = 60
TIME1970 = 2208988800L


def time_to_string(t):
    return time.strftime('%c', t)


def udp_socket():
    return socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


def ntp_request(address):
    client = udp_socket()
    client.sendto('\x1b' + 47 * '\0', address)
    msg, address = client.recvfrom(1024)
    t = struct.unpack("!12I", msg)
    return t


def run_server():
    sock = udp_socket()
    sock.bind(ADDRESS)
    print 'S> NTP server is running'
    while 1:
        data, addr = sock.recvfrom(1024)
        print 'S> Received %d bytes from %s' % (len(data), addr)
        answer = ntp_request(NTP_ORIGIN)
        origin_seconds = answer[10] - TIME1970
        answer = list(answer)
        print 'S> Origin time: %s' % time_to_string(time.localtime(origin_seconds))
        answer[10] += DELTA_SECONDS
        print 'S> Sent time: %s' % time_to_string(time.localtime(origin_seconds + DELTA_SECONDS))
        sock.sendto(struct.pack('!12I', *answer), addr)

def run_client():
    time.sleep(1)
    answer = ntp_request(ADDRESS)
    origin_seconds = answer[10] - TIME1970
    print 'C> Got time: %s' % time_to_string(time.localtime(origin_seconds))


def main():
    t = threading.Thread(target=run_server)
    t.start()
    for i in range(3):
        run_client()


if __name__ == "__main__":
    main()
